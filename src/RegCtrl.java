package src;


import java.sql.*;

public class RegCtrl extends DBConn {
//Denne tror jeg funker halvveis
    public void insertNewMovie(String tittel, String lengde, String lanseringsdato, String beskrivelse, String medie, String video, String utgivelsesselskap, String sesongID) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into Film (Tittel, Lengde, Lanseringsdato, Beskrivelse, Medie, Video, UtgivelsesselskapID, SesongID) values ('" + tittel + "', " + lengde + ", '" + lanseringsdato + "', '" + beskrivelse + "', '" + medie + "', " + video + ", " + utgivelsesselskap + ", " + sesongID + ")";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }
//vet ikke hvordan denne funker
    public void insertRating(String filmID, String brukerID, String rating, String begrunnelse) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into FilmVurdering values (" + filmID + ", " + brukerID + ", '" + begrunnelse + "', " + rating +")";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }

    public void insertRegissor(String filmID, String regissorID) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into RegissOrForFilm values ('" + filmID + "', '" + regissorID + "')";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }

    public void insertFremforer(String filmID, String regissorID) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into FremforerMusikkIFilm values ('" + filmID + "', '" + regissorID + "')";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }

    public void insertKomponist(String filmID, String regissorID) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into KomponistForFilm values ('" + filmID + "', '" + regissorID + "')";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }

    public void insertManusforfatter(String filmID, String regissorID) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into ManusforfatterForFilm values ('" + filmID + "', '" + regissorID + "')";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }

    public void insertSkuespiller(String filmID, String regissorID, String rolle) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "insert into SkuespillerIFIlm (FilmID, PersonID, Rolle) values ('" + filmID + "', '" + regissorID + "', '" + rolle + "')";
            stmt.executeUpdate(query);
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
        }
    }

}
