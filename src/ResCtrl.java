package src;

import java.sql.*;

class ResCtrl extends DBConn {
//Stoler på at denne er good
    public String getRolesOfActor(String actor) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "select Rolle from SkuespillerIFilm natural join Film natural join Person where Navn = '" + actor + "'";
            ResultSet rs = stmt.executeQuery(query);
            String result = "";
            while (rs.next()) {
                result += rs.getString("rolle");
                result += "\n";
            }
            return result;
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
            return null;
        }
    }
// Henter distinkte titler som gitt skuespiller har vært i
    public String getMoviesOfActor(String actor){
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "select distinct Tittel from SkuespillerIFilm natural join Film natural join Person where Navn = '" + actor + "'";
            ResultSet rs = stmt.executeQuery(query);
            String result = "";
            while (rs.next()) {
                result += rs.getString("Tittel");
                result += "\n";
            }
            return result;
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
            return null;
        }
    }
//Tror denne skal printe selskap med flest filmer i en og en sjanger. trenger ikke noe input
    public String mostMoviesInGenre(String category){
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "select Utgivelsesselskap.Navn, COUNT(Kategori.KategoriID) as C from Utgivelsesselskap natural join Film natural join KategoriIFilm join Kategori on Kategori.KategoriID = KategoriIFilm.KategoriID where Kategori.Navn = '" + category + "' group by Utgivelsesselskap.Navn order by count(Kategori.KategoriID) desc";
            ResultSet rs = stmt.executeQuery(query);
            String result = "";
            int max = -1;
            while (rs.next()) {
                if (rs.getInt("C") >= max) {
                    max = rs.getInt("C");
                    result += rs.getString("Utgivelsesselskap.Navn");
                    result += "\n";
                } else {
                    break;
                }
            }
            return result;
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
            return null;
        }
    }

    public String getIdOfMovie(String tittel) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "select FilmID from Film where Tittel = '" + tittel + "'";
            ResultSet rs = stmt.executeQuery(query);
            String result = "";
            if (rs.next()) {
                result = rs.getString("FilmID");
            }
            return result;
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
            return null;
        }
    }

    public String getIdOfPerson(String navn) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "select PersonID from Person where Navn = '" + navn + "'";
            ResultSet rs = stmt.executeQuery(query);
            String result = "";
            if (rs.next()) {
                result = rs.getString("PersonID");
            }
            return result;
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
            return null;
        }
    }

    public String getIdOfCompany(String navn) {
        try {
            this.connect();
            Statement stmt = conn.createStatement();
            String query = "select UtgivelsesselskapID from Utgivelsesselskap where Navn = '" + navn + "'";
            ResultSet rs = stmt.executeQuery(query);
            String result = "";
            if (rs.next()) {
                result = rs.getString("UtgivelsesselskapID");
            }
            return result;
        } catch (Exception e) {
            System.out.println("Something didn't work");
            System.out.println(e);
            return null;
        }
    }
}