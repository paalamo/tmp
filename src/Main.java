package src;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main {

    RegCtrl DBRegister;
    ResCtrl DBGget;

    public Main() {
        DBRegister = new RegCtrl();
        DBGget = new ResCtrl();
        meny();
    }

    public void meny(){
        System.out.println("VELG EN HANDLING:");
        System.out.println("-----------------");
        System.out.println("0: Avslutt program");
        System.out.println("1: Rollene til en gitt skuespiller");
        System.out.println("2: Filmer til en gitt skuespiller");
        System.out.println("3: Filmselskap med flest filmer innen gitt kategori");
        System.out.println("4: Sett inn ny film");
        System.out.println("5: Sett inn annmeldelse av en episode av en serie");
        int svar = Integer.parseInt(getInput());
        if (svar == 0) {
            h0();
        } else if (svar == 1) {
            h1();
        } else if (svar == 2) {
            h2();
        } else if (svar == 3) {
            h3();
        } else if (svar == 4) {
            h4();
        } else if (svar == 5) {
            h5();
        } else {
            System.out.println("Ikke gyldig operator\n");
            meny();
        }
    }

    public void h0() {
        System.out.println("Thanks for using our program");
    }

    public void h1() {
        System.out.println("\nValgt handling: 1. Skriv navn på skuespiller");
        String svar = getInput();
        System.out.println(DBGget.getRolesOfActor(svar));
        meny();
    }

    public void h2() {
        System.out.println("\nValgt handling: 2. Skriv navn på skuespiller");
        String svar = getInput();
        System.out.println(DBGget.getMoviesOfActor(svar));
        meny();
    }

    public void h3() {
        System.out.println("\nValgt handling: 3. Skriv navn på kategori");
        String svar = getInput();
        System.out.println(DBGget.mostMoviesInGenre(svar));
        meny();
    }

    public void h4() {
        System.out.println("\nValgt handling: 4");
        System.out.println("Skriv inn tittel. Format: Tekst");
        String tittel = getInput();
        System.out.println("Skriv inn lengde. Format: Integer");
        String lengde = getInput();
        System.out.println("Skriv inn lanseringsdato. Format: YYYY-MM-DD");
        String lanseringsdato = getInput();
        System.out.println("Skriv inn beskrivelse. Format: Tekst");
        String beskrivelse = getInput();
        System.out.println("Skriv inn medie. Format: Tekst");
        String medie = getInput();
        System.out.println("Skriv inn om utgitt på video. Format: 1=True, 0=False");
        String video = getInput();
        System.out.println("Skriv inn utgivelsesselskap. Format: Tekst");
        String utgivelsesselskap = getInput();
        String utgivelsesselskapID = DBGget.getIdOfCompany(utgivelsesselskap);
        System.out.println("Skriv inn SesongID. Format: Integer (skriv 'null' hvis den ikke tilhører en sesong)");
        String sesongID = getInput();
        DBRegister.insertNewMovie(tittel, lengde, lanseringsdato, beskrivelse, medie, video, utgivelsesselskapID, sesongID);
        String filmID = DBGget.getIdOfMovie(tittel);
        System.out.println("Skriv inn navn på regissør. Format: Tekst");
        String regissorNavn = getInput();
        DBRegister.insertRegissor(filmID, DBGget.getIdOfPerson(regissorNavn));
        System.out.println("Skriv inn navn på komponist. Format: Tekst");
        String komponistNavn = getInput();
        DBRegister.insertKomponist(filmID, DBGget.getIdOfPerson(komponistNavn));
        System.out.println("Skriv inn navn på fremfører. Format: Tekst");
        String fremforerNavn = getInput();
        DBRegister.insertFremforer(filmID, DBGget.getIdOfPerson(fremforerNavn));
        System.out.println("Skriv inn navn på manusforfatter. Format: Tekst");
        String manusforfatterNavn = getInput();
        DBRegister.insertManusforfatter(filmID, DBGget.getIdOfPerson(manusforfatterNavn));
        System.out.println("Skriv inn navn på skuespiller. Format: Tekst. (Trykk enter når du er ferdig)");
        String skuespillerNavn = getInput();
        while (skuespillerNavn.length() != 0) {
            System.out.println("Skriv inn navn på rolle. Format: Tekst.");
            String rolle = getInput();
            DBRegister.insertSkuespiller(filmID, DBGget.getIdOfPerson(skuespillerNavn), rolle);
            System.out.println("Skriv inn navn på skuespiller. Format: Tekst. (Trykk enter når du er ferdig)");
            skuespillerNavn = getInput();
            System.out.println(skuespillerNavn);
        }
        meny();
    }

    public void h5() {
        System.out.println("\nValgt handling: 5. Skriv inn navn på film:");
        String tittel = getInput();
        String filmID = DBGget.getIdOfMovie(tittel);
        System.out.println("Skriv ID på bruker. Format: integer");
        String brukerID = getInput();
        System.out.println("Skriv vurdering på film. Format: integer (1-10)");
        String rating = getInput();
        System.out.println("Skriv begrunnelse av vurdering. Format: tekst");
        String beskrivelse = getInput();
        DBRegister.insertRating(filmID, brukerID, rating, beskrivelse);
        meny();
    }

    public String getInput() {
        try {
            return new BufferedReader(new InputStreamReader(System.in)).readLine();
        } catch (Exception e) {
            System.out.println("Something didn't work");
            return null;
        }
    }

    public static void main(String[] args) {
        Main m = new Main();
    }

}
